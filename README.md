# Plugin WooCommerce Restaurant.pe

Plugin para enviar pedidos de Delivery desde WoCommerce a la API de Restaurant.pe. Repositorio privado https://gitlab.com/diurvan/diu-restaurant-pe

![Imagen](https://gitlab.com/diurvan/plugin-woocommerce-restaurant.pe/-/raw/main/restaurant_pe-1.png)
![Imagen](https://gitlab.com/diurvan/plugin-woocommerce-restaurant.pe/-/raw/main/restaurant_pe-2.png)
![Imagen](https://gitlab.com/diurvan/plugin-woocommerce-restaurant.pe/-/raw/main/restaurant_pe-3.png)
![Imagen](https://gitlab.com/diurvan/plugin-woocommerce-restaurant.pe/-/raw/main/restaurant_pe-4.png)

Este plugin envía información en la consola de administración de Resurant.pe.

Contribuye con tus comentarios.

Visita mi web en https://diurvanconsultores.com
